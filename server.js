var baudrate = 115200;
var webport = 8888;

var portName = "COM4"
var clients = []

var express = require('express');
var app = require('express')();

var http = require('http').Server(app);
var io = require('socket.io')(http);
// ini akan serve index.html saat client get/ data
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/web/index.html');
});
// this will serve "public" static file
app.use(express.static(__dirname + '/web/'));


http.listen(8888, function() {
    console.log('server listening on *:8888');
});
http.listen(webport); // listening to the port

//

var pub = []

// socket io

io.on('connection', function(socket) {
    console.log('client connected with id ' + socket.id);
    socket.on('disconnect', function() {
        console.log('user '+socket.id+' disconnected');
    });
});

// package for database
// package for serial
var serialport = require('serialport');
var commport = []
// connecting to serial port with selected name and baud rate
var myPort = new serialport(portName, {
    baudRate: baudrate, // choose between: 115200, 57600, 38400, 19200, 9600, 4800, 2400, 1800, 1200, 600, 300, 200, 150, 134, 110, 75, or 50
    parser: serialport.parsers.readline("\n")
});
// set callback di event myPort
var raw = []
myPort.on('open', showPortOpen);
myPort.on('data', sendSerialData);
myPort.on('close', showPortClose);
myPort.on('error', showError);

var n = 0;

function showPortOpen() {
    console.log('connected to ' + portName + '. Data rate: ' + myPort.options.baudRate);
    //    setInterval(loop, 10);
}

function sendSerialData(data) {
  // console.log("awesome");
  raw.push(data);
  if (raw.length>100) {
    raw.shift()
  }
  pub = JSON.parse(data)
  io.emit("pub",pub)
}


function showPortClose() {
    console.log('serial port closed.');
}

function showError(error) {
    console.log('Serial port error: ' + error);
}
