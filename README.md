project ini adalah project yang dibuat untuk memenuhi syarat kuliah **Internet of Things** di semester ganjil 2017

dibuat oleh [Fathin luqman Tantowi](http://about.me/fathinluqman)
11/319291/TK/38421


langkah kerja:

[installing node.js](https://nodejs.org/en/download/current/)

https://nodejs.org/en/download/current/

buat file `package.json` untuk membuat deskripsi project

```json
{
  "name": "ecollar",
  "version": "0.0.1",
  "email": "080893lukie@gmail.com",
  "url": "https://about.me/fathinluqman",
  "dependencies": {
    "chart.js": "^2.4.0",
    "express": "^4.14.0",
    "jsonfile": "^2.4.0",
    "nano": "^6.2.0",
    "serialport": "^4.0.7",
    "socket.io": "^1.7.2"
  }
}

```
file ini akan membantu npm untuk mendownload modules yang merupakan dependecy
project serta menyatakan detil project tersebut

lalu download semua dependency yang dibutuhkan
> \> `npm install`

untuk penjelasan source code ada di comment yang ada di source code

dalam project ini dibuat arsitektur system yang kurang lebih sebagai berikut:

sensor clients <=realtime=> server <=realtime=> ui

sensor client (arduino):

```c
#include <Arduino.h>

int a[6] = {0,0,0,0,0,0};

void setup() {
  Serial.begin(115200);
}

void loop() {
  for (int i = 0; i < 6; i++) {
    // dumping {a0:number,a1:number . . .}
    if (i==0) {
      Serial.print("{");
    }
    oa[i] = a[i];
    a[i] = analogRead(i);
    Serial.print("\"a");
    Serial.print(i);
    Serial.print("\":");
    Serial.print(a[i]);
    if (i<5) {
      Serial.print(",");
    }else{
      Serial.print("}\n");
    }
  }
  delay(100);
}
```
client ini mengirimkan data analog ke server dalam bentuk JSON ke server via interface serial.


kode server

sebelum memulai

kita bisa mengirim data ke client dengan menggunakan:

```js
io.emit('pub',variabledata)
```
dimana variabledata bisa berupa string, array, atau bahkan object.

akses node serialport
```js
var portName = "COM4"
var baudrate = 115200;
var myPort = new serialport(portName, {
    baudRate: baudrate,
    parser: serialport.parsers.readline("\n")
});
myPort.on('data', sendSerialData);
function sendSerialData(data) {
  // console.log("awesome");
  raw.push(data);
  if (raw.length>100) {
    raw.shift()
  }
  pub = [JSON.parse(data)]
  io.emit("pub",pub)
}
```
serialport akan membaca stream serial yang dituju, dalam hal ini `COM4` dengen baudrate `115200` dan mengirim event `data` setiap menemui karakter `\n` (newline). lalu fungsi `sendSerialData()` akan parsing JSON struktur yang ada dan membuat object `pub`, dan mengirimkan object tersebut melalui `socket.io`

web client
```js
var sock = io()
data = {}
sock.on('pub',function(msg){
  data = msg
  w3.displayObject("datas", msg);
});
```
web client akan mengubah pattern di `<div id="datas">`
dimana `{{a0}}` akan diisi oleh nilai object `msg.a0`
