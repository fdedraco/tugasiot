/* this is client

*/
// structure = [RPM array], [torque array], [Current array], [voltage array]
/*
read from cookies if there's anys

*/


var time = new Date();
// address
var v = {
    n: 0,
    rpm: 1,
    torque: 2,
    current: 3,
    volt: 4,
    pf: 5,
    mp: 6,
    ep: 7
};

var a = {
    n: 0,
    rpm: 1,
    torque: 2,
    current: 3,
    volt: 4,
    pf: 5,
    mp: 6,
    ep: 7
};

var y = {
    rpm: 'y-axis-1',
    torque: 'y-axis-1',
    current: 'y-axis-1',
    volt: 'y-axis-1',
    pf: 'y-axis-1',
    mp: 'y-axis-2',
    ep: 'y-axis-2'
};

var e = {
    rpm: true,
    torque: true,
    current: true,
    volt: true,
    pf: false,
    mp: true,
    ep: true
};
// the following data is an actual raw data received from server

var data = [{
    timestamp: time,
    isformatted: false,
    iscalculated: false,
    isplotted: false,
    val: {
        rpm: 0,
        torque: 1,
        current: 2,
        volt: 3
    },
    data: [
        [7250, 253, 385, 512],
        [7500, 241, 365, 497],
        [7750, 230, 353, 488],
        [8000, 219, 342, 474],
        [3500, 430, 183, 96],
        [3750, 467, 224, 96],
        [4000, 465, 273, 99],
        [4250, 451, 315, 134],
        [4500, 432, 394, 172],
        [4750, 423, 425, 211],
        [5000, 412, 432, 271],
        [4050, 398, 440, 331],
        [5500, 375, 444, 417],
        [5750, 361, 442, 463],
        [6000, 340, 439, 508],
        [6250, 322, 433, 526],
        [6500, 300, 419, 531],
        [6750, 285, 406, 532],
        [7000, 268, 397, 524],
        [7250, 253, 385, 512],
        [7500, 241, 365, 497],
        [7750, 230, 353, 488],
        [8000, 219, 342, 474]
    ],
    // datar: [],
    // plotn: [],
    // plotr: []
}];



function process(p_start, p_stop) {
    p_stop = p_stop - 1
    //for (var i = 0; i < data.length; i++) {
    i = usedata
    if (data[i].isformatted === true) {} else { // format if not formatted yet
        var t_array = []
        var array = [];
        for (var j = 0; j < data[i].data.length; j++) {
          var packetdrop = false
            for (var prop in v) {
                if (data[i].val[prop] != undefined) {
                    if (parseFloat(data[i].data[j][data[i].val[prop]]) > 10000) {
                        console.log("weird stuff, dropping");
                        packetdrop = true;
                    }
                }
            }
            if (!packetdrop) {
                t_array = [];
                t_array[v.n] = j;
                // setting v.n as count of data
                for (var prop in v) {
                    // for every variables there is
                    if (data[i].val[prop] != undefined) {
                        // place variable in the right place
                        t_array[v[prop]] = parseFloat(data[i].data[j][data[i].val[prop]]);
                    }
                }
                array.push(t_array);
            }
        }
        data[i].data = array; // returning formatted file into place
        data[i].isformatted = true; // setting the flag as formatteds
    }

    if (data[i].iscalculated===true) {} else {
        data[i].data = calcdata(data[i].data); //calculate the calculabe stuff on calcdata()
        data[i].iscalculated = true;
        data[i].datar = data[i].data.slice(); // set the data for sorted
        sortByCol(data[i].datar, v.rpm);
    }
    // if (false) {
  if (data[i].isplotted === true ) {} else {
        data[i].plotr = [];
        data[i].plotn = [];
        for (var props in v) {
            if (data[i].data[0][v[props]] === undefined) {
                console.log(props + " is not available");
            } else {
                data[i].plotn.push();
                var headern = {};
                headern.label = props;
                headern.borderColor = randc();
                headern.backgroundColor = 'rgba(0,0,0,0)';
                headern.yAxisID = y[props];

                headern.data = [];

                var headerr = {};
                headerr.label = props;
                headerr.borderColor = randc();
                headerr.backgroundColor = 'rgba(0,0,0,0)';
                headerr.data = [];
                headerr.yAxisID = y[props];

                for (var k = p_start; k < p_stop; k++) {
                    // only for the existing datas in the array
                    if (data[i].data[k] === undefined) {

                    }else{
                    var nvar = {};
                    nvar.x = data[i].data[k][v.n];
                    nvar.y = data[i].data[k][v[props]];
                    headern.data.push(nvar);

                    var rvar = {};
                    rvar.x = data[i].datar[k][v.rpm];
                    rvar.y = data[i].datar[k][v[props]];
                    headerr.data.push(rvar);
                    }
                }


                data[i].plotr.push(headerr);
                data[i].plotn.push(headern);
            }
        }
    }
};

function randc() {
    var string = 'rgb(' + rand() + ',' + rand() + ',' + rand() + ')';
    return string;
}

function rand() {
    return Math.floor(Math.random() * 256);
}

// processing time

function calcdata(input) {
    for (var i = 0; i < input.length; i++) {
        // mechanical power
        input[i][v.mp] = input[i][v.rpm] * input[i][v.torque] / 5252;
        input[i][v.ep] = input[i][v.volt] * input[i][v.current];
    }
    return input;
}

usedata = 0;

function apply() { //function that called when pressing the apply button
    w3DisplayData("display", {
        "timestamp": data[usedata].timestamp
    });

    //change the displayed range
    start = $('#rmin')[0].value
    end = $('#rmax')[0].value
    console.log("start = " + start + " end = " + end);
    process(start, end);
    console.log("prestuff");
    if ($('#axis')[0].value === "time") {
        scatterChartData.datasets = data[usedata].plotn;
    } else if ($('#axis')[0].value === "rpm") {
        scatterChartData.datasets = data[usedata].plotr;
    }
    window.myScatter.update();
}

var settings = {
    axis: 'time' // time or 'rpm'
};

var sorted = {

};

function sortByCol(arr, colIndex) {
    var input = arr;
    input.sort(sortFunction);

    function sortFunction(a, b) {
        a = a[colIndex];
        b = b[colIndex];
        return (a === b) ? 0 : (a < b) ? -1 : 1;
    }
    return input;
}


var scatterChartData = {
    datasets: []
};

function fill(input) {
    for (var i = 0; i < input.length; i++) {

    }
    var data = {

    };
    scatterChartData.datasets.push(data);
}

var entries = {
    label: ""
};

var ctx = document.getElementById("myChart").getContext("2d");
window.myScatter = Chart.Scatter(ctx, {
    data: scatterChartData,
    options: {
        responsive: true,
        maintainAspectRatio: false,
        hoverMode: 'nearest',
        intersect: true,
        title: {
            display: true,
            text: 'measurement result'
        },
        scales: {
            xAxes: [{
                position: "bottom",
                gridLines: {
                    zeroLineColor: "rgba(0,0,0,1)"
                }
            }],
            yAxes: [{
                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                display: true,
                position: "left",
                id: "y-axis-1",
            }, {
                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                display: true,
                position: "left",
                reverse: true,
                id: "y-axis-2",

                // grid line settings
                gridLines: {
                    drawOnChartArea: false, // only want the grid lines for one axis to show up
                },
            }],
        }
    }
});
$(document).ready(function() {
    // A basic sinusoidal data series.


    gdata = [];
    var t = new Date();
    for (var i = 3; i >= 0; i--) {
        var x = new Date(t.getTime() - i * 1000);
        gdata.push([x, Math.random(), Math.random(), Math.random(), Math.random()]);
    }

    g = new Dygraph(document.getElementById('div_g'), gdata, {
        drawPoints: true,
        showRoller: true,
        valueRange: [0, 4000],
        labels: ['Time', 'rpm', 'torque', 'voltage', 'current']
    });



    $('#rmin')[0].value = 0
    $('#rmax')[0].value = 20
    //  $('#rmin').change(apply())
    //  $('#rmax').change(apply())

    apply();



});


console.log("client code run gracefully");
console.log("( •_•) ( •_•)>⌐■-■ (⌐■_■) #Yeeeaaahhh");
